package homework1;

/**
 *
 * @author Hang
 */
public class Swap2Number {

    private static void swap(int a, int b) {
        a = a + b;
        b = a - b;
        a = a - b;
        System.out.println("a: " + a + " - b: " + b);

    }

    public static void main(String[] args) {
        swap(2, 3);
    }

}
