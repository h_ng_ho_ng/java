package homework1;

/**
 *
 * @author Hang
 */
public class Rounding {
     private static double round(double a){
       return  Math.round(a);
    }
//    down
    private static double roundFloor(double a){
       return  Math.floor(a);
    }
//    up
    private static double roundCeil(double a){
       return  Math.ceil(a);
    }
    
    public static void main(String[] args) {
        System.out.println("a: "+ round(9.4));
        System.out.println("a: "+ roundFloor(9.5));
        System.out.println("a: "+ roundCeil(9.4));
    }
}
